
 (function() {

      var myConnector = tableau.makeConnector();
      myConnector.init = function() {
		          tableau.initCallback();
		          tableau.submit();
		      };
      myConnector.getColumnHeaders = function() {
          var fieldNames = ['id','date_created','title', 'description','bloom_id','posted_by','status','category_id','category_name', "strategic_intent"];

          var fieldTypes = ['string','string','string','string','string','string','string','string','string','string'];
          tableau.headersCallback(fieldNames, fieldTypes);
      }

      myConnector.getTableData = function(lastRecordToken) {

          var index;

          var dataToReturn = [];
          var hasMoreData = true;
          var get_access_token = function(){

            
                       
            $.ajax({ 
                      url: "http://tableau-web-conn-dev.mawv3zr2zp.us-east-1.elasticbeanstalk.com/ofiprojects"
                      //url: "http://localhost:3000/ofiprojects", //URL where the data resides?
                      type: "GET",
                      dataType: 'jsonp',
                      contentType: "application/jsonp",

                    success: function(res){ //Callback for the response object
                      noofRecords = res.length;
                      for(i=0;i<noofRecords;i++){
                        op = res[i];
                        //Parse the server response to TWC Response
                        output = {
                        	'id':op["ID"],
                        	'date_created':op['DateCreated'],
                        	'title':op['Title'], 
                        	'description':op['Description'],
                        	'bloom_id':op['BloomId'],
                        	'posted_by':op['PostedBy'],
                        	'status':op['Status'],
                        	'category_id':op['CategoryId'],
                        	'category_name':op['CategoryName'],
                          'strategic_intent':op['StrInt']           
                             };
                        dataToReturn.push(output); //Populate the array that TWC will return
                         };
                        
                        tableau.dataCallback(dataToReturn,0,false); //Send data to Tableau
                   },
                    error: function(err){
                      console.log(err);
                    }
          })
          }
          get_access_token();
        
           
              
          };

          

           tableau.registerConnector(myConnector);
          //var xhr = getRecords(connectionUri, index);


      }
     

      
		
  
      
  )();



